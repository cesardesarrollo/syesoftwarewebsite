import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ServiceUxuiComponent } from './service-uxui/service-uxui.component';
import { ServiceWebDevelopmentComponent } from './service-web-development/service-web-development.component';
import { ServiceMobileDevelopmentComponent } from './service-mobile-development/service-mobile-development.component';
import { ServiceTestingSoftwareComponent } from './service-testing-software/service-testing-software.component';
import { ServiceExtendedTeamsComponent } from './service-extended-teams/service-extended-teams.component';
import { ServiceTrainingServicesComponent } from './service-training-services/service-training-services.component';
import { MainComponent } from './main/main.component';


const appRoutes: Routes = [
  { path: 'services/user-experience-user-interface-design', component: ServiceUxuiComponent },
  { path: 'services/web-development', component: ServiceWebDevelopmentComponent },
  { path: 'services/mobile-development', component: ServiceMobileDevelopmentComponent },
  { path: 'services/testing-software', component: ServiceTestingSoftwareComponent },
  { path: 'services/extended-teams', component: ServiceExtendedTeamsComponent },
  { path: 'services/training-services', component: ServiceTrainingServicesComponent },
  { path: 'main', component: MainComponent }, 
  { path: '',
    redirectTo: '/main',
    pathMatch: 'full'
  },
  { path: '**', component: MainComponent }
];



@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
