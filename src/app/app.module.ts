import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { ServicesComponent } from './services/services.component';
import { MainComponent } from './main/main.component';

import { AppComponent }       from './app.component';
import { AppRoutingModule }   from './app-routing.module';
import { ServiceUxuiComponent } from './service-uxui/service-uxui.component';
import { ServiceWebDevelopmentComponent } from './service-web-development/service-web-development.component';
import { ServiceMobileDevelopmentComponent } from './service-mobile-development/service-mobile-development.component';
import { ServiceTestingSoftwareComponent } from './service-testing-software/service-testing-software.component';
import { ServiceExtendedTeamsComponent } from './service-extended-teams/service-extended-teams.component';
import { ServiceTrainingServicesComponent } from './service-training-services/service-training-services.component';

@NgModule({
  declarations: [
    AppComponent,
    ServicesComponent,
    MainComponent,
    ServiceUxuiComponent,
    ServiceWebDevelopmentComponent,
    ServiceMobileDevelopmentComponent,
    ServiceTestingSoftwareComponent,
    ServiceExtendedTeamsComponent,
    ServiceTrainingServicesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }



