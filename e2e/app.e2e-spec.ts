import { SyesoftwarePage } from './app.po';

describe('syesoftware App', function() {
  let page: SyesoftwarePage;

  beforeEach(() => {
    page = new SyesoftwarePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
